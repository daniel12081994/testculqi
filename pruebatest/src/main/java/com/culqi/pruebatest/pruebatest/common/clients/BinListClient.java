/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.common.clients;

import com.culqi.pruebatest.pruebatest.client.ApiResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

/**
 *
 * @author Daniel Francisco
 */
public class BinListClient extends AbstractClient<ApiResponse> {

    public BinListClient(ClientConfig config) {
        super(config);
    }

    protected ApiResponse getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, new TypeReference<ApiResponse>() {
        });
    }

}
