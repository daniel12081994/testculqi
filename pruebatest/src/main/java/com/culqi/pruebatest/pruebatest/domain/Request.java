/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Daniel Francisco
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request {

    @JsonProperty("pan")
    private String pan;
    @JsonProperty("exp_year")
    private int expYear;
    @JsonProperty("exp_month")
    private int expMonth;

    @JsonProperty("pan")
    public String getPan() {
        return pan;
    }

    @JsonProperty("pan")
    public void setPan(String pan) {
        this.pan = pan;
    }

    @JsonProperty("exp_year")
    public int getExpYear() {
        return expYear;
    }

    @JsonProperty("exp_year")
    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    @JsonProperty("exp_month")
    public int getExpMonth() {
        return expMonth;
    }

    @JsonProperty("exp_month")
    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

}
