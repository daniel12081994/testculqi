/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.model;

import com.culqi.pruebatest.pruebatest.domain.Request;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.json.JSONObject;

/**
 *
 * @author Daniel Francisco
 */
@Component
public class PruebaTestModel {

    private String TKN = "tkn_live_";

    public String getDateTime() {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return hourdateFormat.format(date);
    }

    public String getConcatDatos(Request request) {
        return TKN + request.getPan() + "-" + request.getExpYear() + "-" + request.getExpMonth();
    }

    public static void startController(Logger logger, String controller, Object obj) {
        logger.info("Inicio " + controller + " Controller");
        JSONObject jsonRequest = new JSONObject(obj);
        logger.info("REQUEST => " + jsonRequest.toString());
    }

    public static void finishController(Logger logger, String controller, Object obj) {
        JSONObject jsonRequest = new JSONObject(obj);
        logger.info("RESPONSE => " + jsonRequest.toString());
        logger.info("Fin " + controller + " Controller");
    }

}
