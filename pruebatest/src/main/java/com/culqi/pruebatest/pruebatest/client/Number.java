/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Daniel Francisco
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Number {
    
    @JsonProperty("length")
    private int length;
    @JsonProperty("luhn")
    private boolean luhn;

    @JsonProperty("length")
    public int getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(int length) {
        this.length = length;
    }

    @JsonProperty("luhn")
    public boolean isLuhn() {
        return luhn;
    }

    @JsonProperty("luhn")
    public void setLuhn(boolean luhn) {
        this.luhn = luhn;
    }
    
}
