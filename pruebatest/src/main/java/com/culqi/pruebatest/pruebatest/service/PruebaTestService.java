/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.service;

import com.culqi.pruebatest.pruebatest.client.ApiResponse;
import com.culqi.pruebatest.pruebatest.common.clients.BinListClient;
import com.culqi.pruebatest.pruebatest.common.clients.ClientConfig;
import com.culqi.pruebatest.pruebatest.common.clients.ClientResult;
import com.culqi.pruebatest.pruebatest.domain.Request;
import com.culqi.pruebatest.pruebatest.domain.Response;
import com.culqi.pruebatest.pruebatest.model.PruebaTestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Daniel Francisco
 */
@Service
public class PruebaTestService {
    
    @Autowired
    private PruebaTestModel model;

    public Response getTokensBin(Request request) {
        Response response = new Response();
        ApiResponse binResponse;
        
        binResponse = getBinlist(request.getPan().substring(0, 6));
        
        response.setBrand(binResponse.getScheme());
        response.setToken(model.getConcatDatos(request));
        response.setCreationDt(model.getDateTime());
        
        return response;
    }

    private ApiResponse getBinlist(String uri) {
        ClientConfig config = new ClientConfig.ClientConfigBuilder().setUrl("https://lookup.binlist.net/"+uri).build();
        BinListClient blc = new BinListClient(config);
        ClientResult<ApiResponse> result = blc.post();
        ApiResponse objBL = result.getResult();
        
        return objBL;
    }
    
}
