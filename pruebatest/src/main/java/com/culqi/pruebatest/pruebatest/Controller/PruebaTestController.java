/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.Controller;

import com.culqi.pruebatest.pruebatest.domain.Request;
import com.culqi.pruebatest.pruebatest.domain.Response;
import com.culqi.pruebatest.pruebatest.model.PruebaTestModel;
import com.culqi.pruebatest.pruebatest.service.PruebaTestService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Daniel Francisco
 */
@RestController
public class PruebaTestController {

    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private PruebaTestService service;
    @Autowired
    private PruebaTestModel model;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tokens", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response getTokens(@RequestBody Request request) {
        model.startController(logger, "getTokens", request);
        Response response = new Response();
        try {
            response = service.getTokensBin(request);
        } catch (Exception e) {
            logger.error("Error getTokens Controller.", e);
        }
        model.finishController(logger, "getTokens", response);
        return response;
    }

}
