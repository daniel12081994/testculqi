/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Daniel Francisco
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    
    @JsonProperty("token")
    private String token;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("creation_dt")
    private String creationDt;
    
    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("creation_dt")
    public String getCreationDt() {
        return creationDt;
    }

    @JsonProperty("creation_dt")
    public void setCreationDt(String creationDt) {
        this.creationDt = creationDt;
    }
    
    
    
}
