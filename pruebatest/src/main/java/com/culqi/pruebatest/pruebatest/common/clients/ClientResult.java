/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.common.clients;

/**
 *
 * @author Daniel Francisco
 */
public class ClientResult<T> {

    private T result;
    private boolean success;

    public ClientResult() {
        super();
        this.success = false;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    
}
