/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.client;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Daniel Francisco
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {
    
    @JsonProperty("number")
    private Number number;
    @JsonProperty("scheme")
    private String scheme;
    @JsonProperty("type")
    private String type;
    @JsonProperty("brand")
    private String brand;
    @JsonProperty("prepaid")
    private boolean prepaid;
    @JsonProperty("country")
    private Country country;
    @JsonProperty("bank")
    private Bank bank;

    @JsonProperty("number")
    public Number getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(Number number) {
        this.number = number;
    }

    @JsonProperty("scheme")
    public String getScheme() {
        return scheme;
    }

    @JsonProperty("scheme")
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    @JsonProperty("prepaid")
    public boolean isPrepaid() {
        return prepaid;
    }

    @JsonProperty("prepaid")
    public void setPrepaid(boolean prepaid) {
        this.prepaid = prepaid;
    }

    @JsonProperty("country")
    public Country getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(Country country) {
        this.country = country;
    }

    @JsonProperty("bank")
    public Bank getBank() {
        return bank;
    }

    @JsonProperty("bank")
    public void setBank(Bank bank) {
        this.bank = bank;
    }
        
}
