/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.common.clients;

/**
 *
 * @author Daniel Francisco
 */
public class ClientConfig {
	private String url;

	public ClientConfig(String url) {
		super();
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public static class ClientConfigBuilder {
		private String url;
				
		public ClientConfigBuilder() {
			super();
		}

		public ClientConfigBuilder setUrl(String url) {
			this.url = url;
			return this;
		}

		public ClientConfig build () {
			return new ClientConfig(url);
		}
	}
}
