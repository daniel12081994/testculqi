/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.common.clients;

import java.net.URI;
import java.util.Date;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Daniel Francisco
 */
public class Api {

    private static final Logger logger = LogManager.getLogger();

    private Api() {
    }

    public static String jerseyGET(URI uri) {
        Date timeStamp = new Date();
        //logger.info("REMOTE_CALL", String.format("%s peticion a %s", timeStamp.getTime(), uri));
        org.glassfish.jersey.client.ClientConfig config = new org.glassfish.jersey.client.ClientConfig();
        logger.info("Request GET: "+uri);

        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(UriBuilder.fromUri(uri).build());

        String response = target.request().accept(MediaType.TEXT_PLAIN).get(String.class).toString();
        logger.info("Response desde "+uri+": "+response);
        //logger.info("REMOTE_CALL", String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, response));
        return response;
    }
}
