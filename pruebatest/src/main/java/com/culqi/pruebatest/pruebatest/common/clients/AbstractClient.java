/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.culqi.pruebatest.pruebatest.common.clients;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author Daniel Francisco
 */
public abstract class AbstractClient<R> {
    
    protected ClientConfig config;

    protected AbstractClient(ClientConfig config) {
        super();
        this.config = config;
    }
    
    public ClientResult<R> post() {
        ClientResult<R> result = new ClientResult<>();

        try {

            String json = doRequest();

            R apiResponse = getResponse(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

        } finally {
            
        }
        return result;
    }
    
    protected String doRequest() throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyGET(uri);
        return jsonSMS;
    }
    
    protected abstract R getResponse(String json) throws Exception;
    
}
